﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survival
{
    public class Ray2D
    {
        private Vector2 startPos;
        private Vector2 endPos;

        public List<Point> points;

        public Ray2D(Vector2 startPos, Vector2 endPos)
        {
            this.startPos = startPos;
            this.endPos = endPos;
        }

        public Ray2D()
        {
            points = new List<Point>();
        }

        /*public void UpdateIntersects(Vector2 startPos, Vector2 endPos)
        {
            points.Clear();

            Point p0 = new Point((int)startPos.X, (int)startPos.Y);
            Point p1 = new Point((int)endPos.X, (int)endPos.Y);

            foreach (Point testPoint in BresenhamLine(p0, p1))
            {
                int x = (testPoint.X + Block.SIZE / 2) / Block.SIZE;
                int y = (testPoint.Y + Block.SIZE / 2) / Block.SIZE;
                Block b = Main.map.Blocks[x, y];
                if (b.Solid && b.Rectangle.Contains(testPoint))
                    break;
                points.Add(testPoint);
            }
        }*/

        public void UpdateIntersects(Vector2 startPos, Vector2 endPos)
        {
            points.Clear();

            Point p0 = new Point((int)startPos.X, (int)startPos.Y);
            Point p1 = new Point((int)endPos.X, (int)endPos.Y);
            List<Point> rayLine = BresenhamLine(p0, p1);
            if (rayLine.Count > 0)
            {
                int rayPointIndex = 0;

                if (rayLine[0] != p0) rayPointIndex = rayLine.Count - 1;

                // Loop through all the points starting from "position"
                while (true)
                {
                    Point rayPoint = rayLine[rayPointIndex];
                    int x = (rayPoint.X + Block.SIZE / 2) / Block.SIZE;
                    int y = (rayPoint.Y + Block.SIZE / 2) / Block.SIZE;
                    if (x < 0 || y < 0 || x >= Map.SIZE || y >= Map.SIZE)
                        break;
                    Block b = Main.map.Blocks[x, y];
                    if (b.Solid && b.Rectangle.Contains(rayPoint))
                    {
                        /*result.Position = Vector.FromPoint(rayPoint);
                        result.DoCollide = true;*/

                        break;
                    }
                    if (rayLine[0] != p0)
                    {
                        rayPointIndex--;
                        points.Add(rayPoint);
                        if (rayPointIndex < 0) break;
                    }
                    else
                    {
                        rayPointIndex++;
                        points.Add(rayPoint);
                        if (rayPointIndex >= rayLine.Count) break;
                    }
                }
            }
        }

        // Swap the values of A and B 

        private void Swap<T>(ref T a, ref T b)
        {
            T c = a;
            a = b;
            b = c;
        }

        // Returns the list of points from p0 to p1  

        private List<Point> BresenhamLine(Point p0, Point p1)
        {
            return BresenhamLine(p0.X, p0.Y, p1.X, p1.Y);
        }

        // Returns the list of points from (x0, y0) to (x1, y1) 

        private List<Point> BresenhamLine(int x0, int y0, int x1, int y1)
        {
            // Optimization: it would be preferable to calculate in
            // advance the size of "result" and to use a fixed-size array
            // instead of a list.
            List<Point> result = new List<Point>();

            bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);
            if (steep)
            {
                Swap(ref x0, ref y0);
                Swap(ref x1, ref y1);
            }
            if (x0 > x1)
            {
                Swap(ref x0, ref x1);
                Swap(ref y0, ref y1);
            }

            int deltax = x1 - x0;
            int deltay = Math.Abs(y1 - y0);
            int error = 0;
            int ystep;
            int y = y0;
            if (y0 < y1) ystep = 1; else ystep = -1;
            for (int x = x0; x <= x1; x++)
            {
                if (steep) result.Add(new Point(y, x));
                else result.Add(new Point(x, y));
                error += deltay;
                if (2 * error >= deltax)
                {
                    y += ystep;
                    error -= deltax;
                }
            }

            return result;
        }
    }
}
