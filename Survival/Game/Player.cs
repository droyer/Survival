﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survival
{
    public class Player
    {
        int width = 32;
        int height = 32;
        Vector2 position;
        Vector2 origin;
        Rectangle rectangle;
        float speed = 300;
        Matrix transform;
        Vector2 centerPosition;
        Map map;
        Point mapIndex;
        Block pointBlock;
        Point mouseIndex;
        Point oldMouseIndex;
        BlockID currentBlockID = BlockID.Dirt;
        float radian;

        Ray2D ray;

        public Matrix Transform
        {
            get { return transform; }
        }

        public Point MapIndex
        {
            get { return mapIndex; }
        }

        public Player(Map map)
        {
            this.map = map;

            origin = new Vector2(width / 2, height / 2);
            centerPosition = new Vector2(Main.screenWidth / 2, Main.screenHeight / 2);

            SetRandomPosition();

            ray = new Ray2D();
        }

        public void UpdateScreen()
        {
            centerPosition = new Vector2(Main.screenWidth / 2, Main.screenHeight / 2);
        }

        public void Update(GameTime gameTime)
        {
            mapIndex = new Point(((int)position.X + Block.SIZE / 2) / Block.SIZE, ((int)position.Y + Block.SIZE / 2) / Block.SIZE);

            UpdateTransform();

            mouseIndex = new Point((Main.mouse.X + (int)position.X + Block.SIZE / 2 - (int)centerPosition.X) / Block.SIZE,
                (Main.mouse.Y + (int)position.Y + Block.SIZE / 2 - (int)centerPosition.Y) / Block.SIZE);

            if (Main.keyboard.IsKeyDown(Keys.D) || Main.keyboard.IsKeyDown(Keys.Right))
                Move(new Vector2((float)gameTime.ElapsedGameTime.TotalSeconds * speed, 0));
            if (Main.keyboard.IsKeyDown(Keys.A) || Main.keyboard.IsKeyDown(Keys.Left) || Main.keyboard.IsKeyDown(Keys.Q))
                Move(new Vector2(-(float)gameTime.ElapsedGameTime.TotalSeconds * speed, 0));
            if (Main.keyboard.IsKeyDown(Keys.W) || Main.keyboard.IsKeyDown(Keys.Up) || Main.keyboard.IsKeyDown(Keys.Z))
                Move(new Vector2(0, -(float)gameTime.ElapsedGameTime.TotalSeconds * speed));
            if (Main.keyboard.IsKeyDown(Keys.S) || Main.keyboard.IsKeyDown(Keys.Down))
                Move(new Vector2(0, (float)gameTime.ElapsedGameTime.TotalSeconds * speed));

            if (Main.PressKey(Keys.F1))
                Map.DEBUG = !Map.DEBUG;

            if (Main.PressKey(Keys.P))
                map.daying = !map.daying;

            if (Main.wheelValue < Main.oldWheelValue)
                currentBlockID++;
            if (Main.wheelValue > Main.oldWheelValue)
                currentBlockID--;
            if ((int)currentBlockID > 3)
                currentBlockID = (BlockID)3;
            if ((int)currentBlockID < 1)
                currentBlockID = (BlockID)1;

            if (!IsOutsideWorld(mouseIndex) && Main.active)
            {
                if (Main.mouse.LeftButton == ButtonState.Pressed)
                {
                    if (mouseIndex != oldMouseIndex || (mouseIndex == oldMouseIndex && Main.lastMouse.LeftButton == ButtonState.Released))
                        map.TryDestroy(mouseIndex);
                }
                if (Main.mouse.RightButton == ButtonState.Pressed)
                    map.TryNewBlock(map.GetNewBlock(currentBlockID, map, mouseIndex.X, mouseIndex.Y));
            }

            if (!IsOutsideWorld(mouseIndex))
                pointBlock = map.Blocks[mouseIndex.X, mouseIndex.Y];
            else
                pointBlock = null;

            Vector2 mousePosition = Main.mousePosition + position - centerPosition;
            Vector2 direction = mousePosition - position;
            radian = (float)Math.Atan2(direction.Y, direction.X);

            ray.UpdateIntersects(position, mousePosition);

            oldMouseIndex = mouseIndex;
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Point p in ray.points)
            {
                spriteBatch.Draw(Main.rectTexture, new Rectangle(p.X, p.Y, 1, 1), Color.White);
            }

            if (pointBlock != null && Main.active)
                spriteBatch.Draw(Main.rectTexture, pointBlock.Rectangle, new Color(50, 50, 50, 50));

            spriteBatch.Draw(Main.playerTexture, position, null, Color.White, radian, origin, 1, SpriteEffects.None, 0);
        }

        public void Draw2D(SpriteBatch spriteBatch)
        {
            string t1 = "X: " + mapIndex.X;
            string t2 = "Y: " + mapIndex.Y;
            float w = 0;
            if (t1.Length > t2.Length)
                w = Main.font.MeasureString(t1).X;
            else
                w = Main.font.MeasureString(t2).X;

            spriteBatch.Draw(Main.rectTexture, new Rectangle(0, 0, (int)Math.Ceiling(w), Main.font.LineSpacing * 2), Color.BlanchedAlmond);

            spriteBatch.DrawString(Main.font, t1, Vector2.Zero, Color.Black);
            spriteBatch.DrawString(Main.font, t2, new Vector2(0, Main.font.LineSpacing), Color.Black);

            spriteBatch.Draw(Main.rectTexture, new Rectangle(Main.screenWidth - Block.SIZE - 20, 10, Block.SIZE + 10, Block.SIZE + 10), Color.BlanchedAlmond);
            spriteBatch.Draw(Main.blockTexture[currentBlockID.ToString()], new Vector2(Main.screenWidth - Block.SIZE - 15, 15), null, Color.White,
                0f, Vector2.Zero, 1, SpriteEffects.None, 0);
        }

        void Move(Vector2 velocity)
        {
            position += velocity;
            UpdateTransform();

            foreach (Block b in map.VisibleBlock)
            {
                if (rectangle.Intersects(b.Rectangle) && b.Solid)
                {
                    velocity.Normalize();

                    if (velocity.X > 0)
                        position.X = b.Position.X - b.Origin.X - origin.X;
                    else if (velocity.X < 0)
                        position.X = b.Position.X + b.Origin.X + origin.X;

                    if (velocity.Y > 0)
                        position.Y = b.Position.Y - b.Origin.X - origin.Y;
                    else if (velocity.Y < 0)
                        position.Y = b.Position.Y + b.Origin.Y + origin.Y;

                    UpdateTransform();
                    break;
                }
            }
        }

        void UpdateTransform()
        {
            rectangle = new Rectangle((int)position.X - (int)origin.X, (int)position.Y - (int)origin.Y, width, height);
            transform = Matrix.CreateTranslation(new Vector3(-position + centerPosition, 0));
        }

        public void SetPosition(Vector2 position)
        {
            this.position = position;
        }

        bool IsOutsideWorld(Point point)
        {
            if (point.X < 0 || point.Y < 0 || point.X >= Map.SIZE || point.Y >= Map.SIZE)
                return true;
            return false;
        }

        void SetRandomPosition()
        {
            Block[,] blocks = map.Blocks;

            while (true)
            {
                int x = map.Random.Next(0, Map.SIZE);
                int y = map.Random.Next(0, Map.SIZE);

                Block b = blocks[x, y];
                if (!b.Solid)
                {
                    position = b.Position;
                    break;
                }
            }
        }
    }
}
