﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace Survival
{
    public struct MapInfo
    {
        public int nb_block;
        public float mulPer;
        public int offset;
    }

    public enum BlockID
    {
        None = 0,
        Dirt = 1,
        Stone = 2,
        Torch = 3
    }

    public class Map
    {
        public static int SIZE = 100;
        public static bool DEBUG;

        public bool daying = true;

        Random random;

        MapInfo info;
        Player player;
        Block[,] blocks;
        bool ready;

        public float lightAlpha = 0;
        
        List<Block> visibleBlock = new List<Block>();
        List<BlockTorch> torchs = new List<BlockTorch>();

        Point mapHalfIndex;

        Thread generateThread;

        public Random Random
        {
            get { return random; }
        }

        public float LightAlpha
        {
            get { return lightAlpha; }
        }

        public MapInfo Info
        {
            get { return info; }
        }

        public List<Block> VisibleBlock
        {
            get { return visibleBlock; }
        }

        public Block[,] Blocks
        {
            get { return blocks; }
        }

        public Map()
        {
            mapHalfIndex = new Point(Main.screenWidth / Block.SIZE / 2 + 2,
                Main.screenHeight / Block.SIZE / 2 + 2);

            random = new Random();

            generateThread = new Thread(Generate);
            generateThread.Start();
        }

        public void UpdateScreen()
        {
            mapHalfIndex = new Point(Main.screenWidth / Block.SIZE / 2 + 2,
                Main.screenHeight / Block.SIZE / 2 + 2);
            if (player != null)
                player.UpdateScreen();
        }

        public void Generate()
        {
            blocks = new Block[SIZE, SIZE];
            info.nb_block = SIZE * SIZE;
            info.mulPer = 100.0f / info.nb_block;

            for (int y = 0; y < SIZE; y++)
            {
                for (int x = 0; x < SIZE; x++)
                {
                    blocks[x, y] = new BlockDirt(this, x, y);

                    if (x == 0 || y == 0 || x == SIZE - 1 || y == SIZE - 1)
                        blocks[x, y] = new BlockStone(this, x, y);
                    else
                    {
                        if (random.Next(0, 10) == 0)
                        {
                            blocks[x, y] = new BlockStone(this, x, y);
                            blocks[x, y].oldId = BlockID.Dirt;
                        }
                    }
                    
                    info.offset++;
                }
            }

            player = new Player(this);
            Main.menu = null;
            ready = true;
        }

        public void Update(GameTime gameTime)
        {
            if (!ready) return;
            if (player == null) return;

            visibleBlock.Clear();
            for (int y = player.MapIndex.Y - mapHalfIndex.Y; y < player.MapIndex.Y + mapHalfIndex.Y; y++)
            {
                for (int x = player.MapIndex.X - mapHalfIndex.X; x < player.MapIndex.X + mapHalfIndex.X; x++)
                {
                    if (x < 0 || x >= SIZE || y < 0 || y >= SIZE)
                        continue;
                    blocks[x, y].Update(gameTime);
                    visibleBlock.Add(blocks[x, y]);
                }
            }

            player.Update(gameTime);

            if (daying)
                lightAlpha -= (float)gameTime.ElapsedGameTime.TotalSeconds * 225 / 1;
            else
                lightAlpha += (float)gameTime.ElapsedGameTime.TotalSeconds * 225 / 1;

            if (lightAlpha <= 0)
                lightAlpha = 0;
            if (lightAlpha >= 235)
                lightAlpha = 235;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!ready) return;
            if (player == null) return;

            spriteBatch.Begin();
            spriteBatch.Draw(Main.rectTexture, new Rectangle(0, 0, Main.screenWidth, Main.screenHeight), Color.Lerp(Color.LightCyan, Color.Black, lightAlpha / 200)); 
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp,
                DepthStencilState.None, RasterizerState.CullNone, null, player.Transform);

            foreach (Block b in visibleBlock)
                b.Draw(spriteBatch);

            player.Draw(spriteBatch);

            foreach (Block b in visibleBlock)
                b.DrawMask(spriteBatch);

            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone);

            player.Draw2D(spriteBatch);

            spriteBatch.End();
        }

        public void TryNewBlock(Block newBlock)
        {
            Point p = newBlock.Index;
            Block currentBlock = blocks[p.X, p.Y];
            if (newBlock.ID == currentBlock.ID)
                return;

            if (!newBlock.Solid && currentBlock.Solid)
                return;
            if (newBlock.Solid && currentBlock.ID == BlockID.Torch)
                return;
            if (!newBlock.Solid && currentBlock.ID == BlockID.Torch)
            {
                currentBlock.oldId = newBlock.ID;
                return;
            }
            if (newBlock.ID == BlockID.Torch)
                torchs.Add((BlockTorch)newBlock);

            newBlock.oldId = currentBlock.ID;
            blocks[p.X, p.Y] = newBlock;
            newBlock.OnPlaced();
        }

        public void TryDestroy(Point p)
        {
            Block currentBlock = blocks[p.X, p.Y];
            Block newBlock = GetNewBlock(currentBlock.oldId, this, p.X, p.Y);
            if (currentBlock == newBlock)
                return;
            if (currentBlock.ID == BlockID.Torch)
                torchs.Remove((BlockTorch)currentBlock);
            blocks[p.X, p.Y] = newBlock;
            currentBlock.OnDestroyed();
        }

        public Block GetNewBlock(BlockID id, Map map, int x, int y)
        {
            if (id == BlockID.None)
                return new BlockNone(map, x, y);
            else if (id == BlockID.Dirt)
                return new BlockDirt(map, x, y);
            else if (id == BlockID.Stone)
                return new BlockStone(map, x, y);
            else if (id == BlockID.Torch)
                return new BlockTorch(map, x, y);
            return null;
        }

        public void UpdateLight()
        {
            foreach(BlockTorch t in torchs)
                t.UpdateNoLight();
            foreach (BlockTorch t in torchs)
                t.CalcLight();
        }
    }
}
