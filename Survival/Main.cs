﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Survival
{
    public class Main : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public static int screenWidth = 800;
        public static int screenHeight = 600;
        public static Texture2D rectTexture;
        public static SpriteFont font;
        public static MouseState mouse;
        public static MouseState lastMouse;
        public static KeyboardState keyboard;
        public static KeyboardState lastKeyboard;
        public static Rectangle mouseRectangle;
        public static Vector2 mousePosition;
        public static bool exit;
        public static bool active;
        public static int wheelValue;
        public static int oldWheelValue;

        public static Texture2D buttonTexture;
        public static Texture2D backgroundTexture;
        public static Texture2D playerTexture;
        public static Dictionary<string, Texture2D> blockTexture;

        public static Menu menu;
        public static Map map;

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            int w = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            int h = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;
            IsMouseVisible = true;
            graphics.IsFullScreen = false;
            Window.AllowUserResizing = true;
            System.Windows.Forms.Form form = (System.Windows.Forms.Form)System.Windows.Forms.Control.FromHandle(Window.Handle);
            form.Resize += Form_Resize;
            Window.Position = new Point(w / 2 - screenWidth / 2, h / 2 - screenHeight / 2);
        }

        private void Form_Resize(object sender, EventArgs e)
        {
            screenWidth = Window.ClientBounds.Width;
            screenHeight = Window.ClientBounds.Height;
            if (screenWidth < 800)
                screenWidth = 800;
            if (screenHeight < 600)
                screenHeight = 600;

            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;
            graphics.ApplyChanges();

            if (menu != null)
                menu.UpdatePosition();
            if (map != null)
                map.UpdateScreen();
        }

        protected override void Initialize()
        {
            

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            rectTexture = new Texture2D(GraphicsDevice, 1, 1);
            rectTexture.SetData(new Color[] { Color.White });

            font = Content.Load<SpriteFont>("Font");

            buttonTexture = Content.Load<Texture2D>("Gui/Button");
            backgroundTexture = Content.Load<Texture2D>("Gui/Background");
            playerTexture = Content.Load<Texture2D>("Player");

            blockTexture = new Dictionary<string, Texture2D>();
            string[] files = Directory.GetFiles("Content/Blocks", "*.xnb");
            for (int i = 0; i < files.Length; i++)
            {
                string textureName = files[i].Split('\\', '.')[1];
                blockTexture.Add(textureName, Content.Load<Texture2D>("Blocks/" + textureName));
            }

            menu = new MenuMain();
            //map = new Map();
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            keyboard = Keyboard.GetState();
            mouse = Mouse.GetState();
            wheelValue = mouse.ScrollWheelValue;

            active = IsActive;

            if (Keyboard.GetState().IsKeyDown(Keys.Escape) || exit)
                Exit();

            mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1);
            mousePosition = new Vector2(mouse.X, mouse.Y);

            if (menu != null)
                menu.Update(gameTime);
            if (map != null)
                map.Update(gameTime);

            base.Update(gameTime);

            lastKeyboard = keyboard;
            lastMouse = mouse;
            oldWheelValue = wheelValue;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            if (menu != null)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.None,
                    RasterizerState.CullNone);

                menu.Draw(spriteBatch);

                spriteBatch.End();
            }
            if (map != null)
                map.Draw(spriteBatch);

            base.Draw(gameTime);
        }

        public static bool PressKey(Keys key)
        {
            if (keyboard.IsKeyDown(key) && lastKeyboard.IsKeyUp(key))
                return true;
            return false;
        }
    }
}
